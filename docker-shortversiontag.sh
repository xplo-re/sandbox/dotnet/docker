#!/usr/bin/env sh

# Extracts a short version tag from version.txt by reducing each component
# version to "<major>.<minor>" only. This also includes pre-releases.

set -o pipefail

if test ! -r version.txt;
then
    echo "error: version.txt not found or readable, artifact missing?" >&2
    exit 1
fi

VersionTag=$( cat version.txt )
ShortVersionTag=

for version in $( echo "$VersionTag" | tr "-" " " );
do
    shortVersion=$( echo "$version" | cut -d . -f 1,2 )

    if test -z "$ShortVersionTag";
    then
        ShortVersionTag="$shortVersion"
    else
        ShortVersionTag="$ShortVersionTag-$shortVersion"
    fi
done

echo "$ShortVersionTag"
