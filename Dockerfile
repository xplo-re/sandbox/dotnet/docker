# Use latest runtime image as baseline, which may include environment definitions.
# https://mcr.microsoft.com/v2/dotnet/runtime/tags/list
FROM mcr.microsoft.com/dotnet/runtime:5.0-bullseye-slim AS baseline

# Add runtimes from official runtime images.
# https://mcr.microsoft.com/v2/dotnet/runtime/tags/list
FROM mcr.microsoft.com/dotnet/runtime:2.1-stretch-slim  AS dotnet-2.1
FROM mcr.microsoft.com/dotnet/runtime:3.1-bullseye-slim AS dotnet-3.1

# Copy host/fxr/ and runtimes from shared/ directory; each contain per-version
# subdirectories and will not overwrite other files. Filtering via .dockerignore
# is not an option as it is not supported in combination with --from.
FROM baseline

COPY --from=dotnet-2.1 ["/usr/share/dotnet/host",   "/usr/share/dotnet/host/"]
COPY --from=dotnet-2.1 ["/usr/share/dotnet/shared", "/usr/share/dotnet/shared/"]

COPY --from=dotnet-3.1 ["/usr/share/dotnet/host",   "/usr/share/dotnet/host/"]
COPY --from=dotnet-3.1 ["/usr/share/dotnet/shared", "/usr/share/dotnet/shared/"]
