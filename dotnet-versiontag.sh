#!/usr/bin/env bash

# Parses 'dotnet --list-runtimes' and outputs a single version identifier string
# with each runtime version concatenated by a dash, from newest to oldest.
# Dashes in runtime versions (e.g. pre-releases) are replaced by an underscore.

set -o pipefail

# Remove newline at end of ordered runtime versions list.
echo -n $(
    dotnet --list-runtimes |
    # Only keep .NET app runtimes.
    grep "Microsoft.NETCore.App" |
    # Extract version column.
    cut -s -d" " -f2 |
    # Sort in reverse order, newest runtime is always listed first.
    sort --version-sort --reverse
    ) |
    # Preview versions have additional version information, separated by a dash.
    # Replace by underscore and concatenate versions with a dash.
    # Docker tags only allow alphanumeric characters, dashes, dots and underscores.
    tr "-" "_" |
    tr " " "-"

# Add final newline.
echo